package no.dkit.kafkaclient.sender;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrphanPaymentRequest {
    public enum INDICATOR {CREDIT, DEBIT}

    String pspName;
    String acquirerName;
    long clearingReference;
    String providerPaymentReference;
    String phoneNumber;
    BigDecimal amount;
    String currency;
    INDICATOR debitCreditIndicator;
}
