package no.dkit.kafkaclient.sender;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import no.dkit.kafkaclient.receiver.OrphanPaymentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
public class ResponseSender {
    @Value("${kafka.topic.response}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    Gson gson;

    public void send(OrphanPaymentResponse response) {
        final String key = "Key-" + System.currentTimeMillis();
        final String jsonPayload = gson.toJson(response);
        log.info("sending to topic={}:\n{}\n{}\n", topicName, key, jsonPayload);

        kafkaTemplate.send(topicName, key, jsonPayload)
            .addCallback(
                sendResult -> log.info("Send successfull for key {} invoice {}", key, response.getProviderPaymentReference()),
                throwable -> log.error("Send failed for key {} invoice {}", key, response.getProviderPaymentReference()));
    }
}
