package no.dkit.kafkaclient;

import lombok.extern.slf4j.Slf4j;
import no.dkit.kafkaclient.receiver.OrphanPaymentResponse;
import no.dkit.kafkaclient.sender.OrphanPaymentRequest;
import no.dkit.kafkaclient.sender.RequestSender;
import no.dkit.kafkaclient.sender.ResponseSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

@SpringBootApplication
@Slf4j
public class KafkaApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class);
    }

    @Autowired
    private RequestSender requestSender;

    @Autowired
    private ResponseSender responseSender;

    @Override
    public void run(String... args) throws Exception {
        // Run until killed - don't do this in production :-)
        try {
            while (true) {
                Thread.sleep(5000);

                for (int i = 0; i < 3; i++) {
                    requestSender.send(new OrphanPaymentRequest(
                        "SWISH", "DABSE", 650531, "9056A0C8674043A39F6E5BA789DEEC26",
                        "46746683711", BigDecimal.valueOf(100.0d), "NOK", OrphanPaymentRequest.INDICATOR.DEBIT
                    ));

                    responseSender.send(new OrphanPaymentResponse(
                        0, "9056A0C8674043A39F6E5BA789DEEC26", "Some message..."
                    ));

                    Thread.sleep(2000);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
