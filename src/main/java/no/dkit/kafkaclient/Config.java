package no.dkit.kafkaclient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    public Gson gson() {
        return new GsonBuilder().create();
    }
}
