package no.dkit.kafkaclient.receiver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrphanPaymentResponse {
    int status;
    String providerPaymentReference;
    String message;
}
