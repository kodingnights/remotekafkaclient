package no.dkit.kafkaclient.receiver;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import no.dkit.kafkaclient.sender.OrphanPaymentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

@Slf4j
public class RequestReceiver {
    @Autowired
    Gson gson;

    public static String topic = "Hello";

    @KafkaListener(topics = "#{requestReceiver.topic}", containerGroup = "myGroup")
    public void receive(
        @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String receivedKey,
        @Header(KafkaHeaders.RECEIVED_TIMESTAMP) String when,
        @Payload String payload) {
        log.info("Consumer {} - Received payload='{}={} at {}'", receivedKey, payload, when);
        OrphanPaymentRequest request = gson.fromJson(payload, OrphanPaymentRequest.class);
        log.info("Payload:\n{}\n", request.toString());
    }
}
